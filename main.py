from utils import get_logger
import pandas as pd
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from kmeans import assign_each_observation_to_nearest_centroid, compute_centroids, get_init_centroids, k_means_plus_plus
from sklearn.metrics import adjusted_rand_score, homogeneity_score, completeness_score, v_measure_score, adjusted_mutual_info_score, silhouette_score
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_blobs, make_circles, make_moons
from sklearn.cluster import KMeans, SpectralClustering

def compute_metrics(x_train, y_train, y_pred_train):
	return {
		"ARI": adjusted_rand_score(y_train, y_pred_train),
		"Completeness": completeness_score(y_train, y_pred_train),
		"Homogeneity": homogeneity_score(y_train, y_pred_train),
		"V-measure": v_measure_score(y_train, y_pred_train),
		"AMI": adjusted_mutual_info_score(y_train, y_pred_train),
		"Silhouette": silhouette_score(x_train, y_pred_train)
	}

def draw_data(X_test, y_test, name):
	tmp = pd.DataFrame(data=X_test)
	tmp['y'] = np.asarray(y_test)
	g = sns.pairplot(tmp, hue='y', markers='*')
	g.savefig(name)


logger = get_logger("CLUSTERINZATION")
def	clusterization(df, goal_field, name, count_of_clusters, draw_plots = False):
	logger.info(f"Кластеризация датасета {name}. Кол-во кластеров {count_of_clusters}")
	observations = df.drop([goal_field],axis=1).to_numpy()
	CLASSES = np.sort(df[goal_field].unique())
	for index, c in enumerate(CLASSES):
		df.loc[df[goal_field] == c, goal_field] = index

	CLASSES = df[goal_field].unique()

	X_train = observations
	y_true = df[goal_field]

	logger.info("Поиск центра кластеров.")
	centroids, y_train_pred, inertia = k_means_plus_plus(X_train, count_of_clusters)
	logger.info("Вычисление центров кластеров завершено.")

	logger.info("Подсчет метрик...")
	metrics_on_train = compute_metrics(X_train, y_true, y_train_pred)
	metrics_on_train['sum of squared distance'] = inertia 

	if draw_plots:
		# draw_data(X_train,y_true,f"{name}-{count_of_clusters}-classes-train.png")
		draw_data(X_train,y_train_pred,f"{name}-{count_of_clusters}-classes-train-pred.png")
	
	return metrics_on_train 

def compute_optim_k(df, goal_field, name, end=10, draw_plots=False):
	metrics = []
	for i in range(2,end):
		data = df.copy()
		metrics_on_train = clusterization(data, goal_field, name, count_of_clusters=i, draw_plots=draw_plots)
		metrics_on_train['count_of_clusters'] = i
		metrics.append(pd.DataFrame([metrics_on_train]))
		print(i)

	metrics = pd.concat(metrics,ignore_index=True)
	metrics['sum of squared distance'] = metrics['sum of squared distance'] / metrics['sum of squared distance'].max()

	print(f'{name} metrics')
	print(metrics)
	# metrics['sum of squared distance'] /= metrics['sum of squared distance'].max()
	df = metrics.melt('count_of_clusters', var_name='cols',  value_name='vals')
	g = sns.catplot(x="count_of_clusters", y="vals", hue='cols', data=df, kind='point')
	g.savefig(f'{name}-metrics.png')

def main():
	print('Iris')
	df = pd.read_csv('datasets/Iris.csv')
	df = df.drop("Id",axis=1)
	data = df.copy()
	goal_field = 'Species'
	name = 'Iris'
	metrics_on_train = clusterization(data, goal_field, name, count_of_clusters=3, draw_plots=True)
	print(pd.DataFrame([metrics_on_train]).round(2))


	# print('Iris')
	# df = pd.read_csv('datasets/Iris.csv')
	# df = df.drop("Id",axis=1)
	# draw_data(df.drop("Species", axis=1).to_numpy(),df["Species"].to_numpy(),"Iris.png")
	# compute_optim_k(df,'Species','Iris',10, True)

	# print('Wine')
	# df = pd.read_csv('datasets/Wine.csv')
	# df_filtered =  df[['Wine','Color.int','Hue','OD','Proline']]
	# X = df_filtered.drop('Wine', axis=1)
	# y_true = df["Wine"].to_numpy()
	# draw_data(X, y_true,"Wine.png")
	# compute_optim_k(df_filtered,'Wine','Wine',10, True)

	# print('Blobs-6')
	# n_samples = 4000
	# n_components = 6
	# X, y_true = make_blobs(n_samples=n_samples,
    #                    centers=n_components,
    #                    cluster_std=0.60,
    #                    random_state=0)
	# draw_data(X,y_true,"Blobs-6.png")
	# df = pd.DataFrame(X)
	# df['y'] = y_true
	# compute_optim_k(df,'y','Blobs-6',10)

	# print('Moons')
	# n_samples = 4000
	# X, y_true = make_moons(n_samples=n_samples,
    #                    noise=.1,
    #                    random_state=0)
	# draw_data(X,y_true,"Moons.png")
	# df = pd.DataFrame(X)
	# df['y'] = y_true
	# compute_optim_k(df,'y','Moons',10, True)

	# print('Circles')
	# n_samples = 4000
	# X, y_true = make_circles(n_samples)
	# draw_data(X,y_true,"Circles.png")
	# df = pd.DataFrame(X)
	# df['y'] = y_true
	# compute_optim_k(df,'y','Circles',10, True)

if __name__ == "__main__":
	main()